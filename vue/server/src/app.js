const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const taskRouter = require('../routers/task')
const jsonParser = express.json()
var conn = 'mongodb://127.0.0.1:27017/task-manager-api'

mongoose.connect(conn, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
})
const app = express()
const middleware = [
    cors(),
    jsonParser,
    taskRouter
    
]
app.use(middleware)
app.listen(3000, ()=>{
    console.log('Добро пожаловать на рисовые поля')
})