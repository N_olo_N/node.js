const mongoose = require('mongoose')
const Schema = new mongoose.Schema({
    description:{
        type:String,
        required:false,
        trim:true
    },
    completed:{
        type:Boolean,
        default: true
    },
    title:{
        type:String,
        required:false
    },
    owner:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    }
})
Schema.methods.getOwner =  async function() {
    const task = this
    await task.populate('owner').execPopulate()
    return task.owner
}
const Task = mongoose.model('Task', Schema)
module.exports = Task

