const express = require('express')
const Task = require('../model/task')
const auth = require('../middleware/auth');
const router = express.Router()

router.get("/task", async (req, res) =>{ // YES
    try{
        let task = await Task.find()
        res.status(200).send(task)
    }catch(err){
        res.status(500).send(err.message)
    }
})


router.get("/task/:id", async (req, res) =>{ // YES
    try{
        let task = await Task.findById(req.params.id)
        res.status(200).send(task)
    }catch(err){
        res.status(500).send(err.message)
    }
})

router.post("/task", (req, res) =>{ // YES
    const task = new Task(req.body)

    task.save().then(() => {
    res.status(201).send(task)
    }).catch((err) => {
        res.status(401).send(err.message)
    })
})

router.delete("/task/:id", (req, res) =>{ // YES
   const id = req.params.id 
   Task.findByIdAndDelete(id).then((task) => {
       res.status(200).send("Task delete")
   }).catch((err) => {
       res.status(400).send(err.message)
   })
} )

// router.put("/task", (req, res) => { // NO
//     const id = req.body._id 
//     const task = new Task(req.body)
//     const updateObject = {
//         new:true,
//         useFindAndModify:false
//     }
    
//     Task.findByIdAndUpdate(id, task, {
//         new:true,
//         useFindAndModify:false
//     }).then(()=>{
//         res.status(200).send("Task update")
//     }).catch((err)=>{
//         res.status(400).send(err.message)
//     })
// })

// lab 4

router.post("/tasks", auth, async (req, res) => {
    const task = new Task({
        ...req.body,
        owner: req.user.id
    });
    try{
        await task.save();
        res.status(201).send(task);
    }catch(e){
        res.status(500).send(e.message);
    }
});

// Додаткови завдання

// 1) Якщо користувач занаходить СВОЄ завдання то це завдання надсилається користувачу (200)
// 2) Якщо користувач знаходить не НЕ СВОЄ завдання то надсилається "This is not your task!" (403)
// 30 Якщо користвач шукає не існуюче завдання то "Task not found!" (404)
router.get("/tasks/:id", auth, async (req, res) => {
    const taskId = req.params.id;
    const userId = req.user._id;
    
    try{
        let task = await Task.findById(taskId)
        const taskOwner = task.owner;

        if(taskOwner.toString() == userId.toString()){
            res.status(200).send(task)
        }else{
            res.status(403).send("This is not your task!")
        } 
    }catch(err){
        res.status(404).send("Task not found!")
    }
});

// 1) Є хоча би один запис у користувача (200)
// 2) Немає ні одного запису (404)
// 3) Помилка на сервері (500)
router.get("/tasks", auth, async (req, res) => {
    const taskId = req.params.id;
    const userId = req.user._id;

    let allUserTasks = [];
    
    try{
        const tasks = await Task.find()

        tasks.forEach(tasks => {
            if(tasks.owner == userId.toString()){
                allUserTasks.push(tasks);
            }
        });

        if(allUserTasks.length == 0){
            res.status(404).send("Tasks not found!");
        }else{
            res.status(200).send(allUserTasks);
        }
    }catch(err){
        res.status(500).send("Error")
    }
});
 

// 1) Оновлення завдання вийшло (200) - ОК!
// 2) Оновлення завдання не вийшло (400) - можливо якість дані були введені некоректно
// 3) Не ваше завдання (403) - авторизуйтеся під правильний користувачем, токен
// 4) Такого завдання не існує (404) - введіть коректний id завдання
router.put("/task", auth, async (req, res) => {
    const taskId = req.body.taskId;
    const userId = req.user._id;  
    
    try{
        const task = await Task.findById(taskId)
        const taskOwner = task.owner;

        if(taskOwner.toString() == userId.toString()){
                Task.findByIdAndUpdate(taskId, req.body, {
                    new:true,
                    useFindAndModify:false
                }).then(()=>{
                    res.status(200).send("Task update")
                }).catch((err)=>{
                    res.status(400).send(err.message)
                })
        }else{
            res.status(403).send("This is not your task!")
        } 
    }catch(err){
        res.status(404).send("Task not found!")
    }

    /////
    // const id = req.body._id 
    
    // try{
    //     const user = await User.findById(id)
    //     const update = ['name', 'email', 'password', 'age']
    //     update.forEach((update) => user[update] = req.body[update])
    //     await user.save()
    //     res.status(200).send("User update")
    // }catch(err){
    //     res.status(400).send(err.message)
    // }
});




module.exports = router;