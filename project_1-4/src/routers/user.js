const express = require('express')
const User = require('../model/user')
const router = express.Router()
const auth = require('../middleware/auth');

router.get('/', (req, res) => {  // req - дани запроса res - дани відповідіnode
    res.send("Hello, Express");
});


router.get("/users", async (req, res) =>{ // Новий метод
    try{
        let users = await User.find()
        res.status(200).send(users)
    }catch(err){
        res.status(500).send(err.message)
    }
})


router.get('/users/me', auth, async(req, res) => {
    res.send(req.user);
})



router.post("/users", async (req, res) =>{
    // const user = new User(req.query.id) 
    const user = new User(req.body)
    const token = await user.generateAuthToken(); 

    user.save().then(() => {
    res.status(201).send(user)
    }).catch((err) => {
        res.status(401).send(err.message)
    })
    
})



router.put("/users", async (req, res) => { // save | put
    const id = req.body._id 
    
    try{
        const user = await User.findById(id)
        const update = ['name', 'email', 'password', 'age']
        update.forEach((update) => user[update] = req.body[update])
        await user.save()
        res.status(200).send("User update")
    }catch(err){
        res.status(400).send(err.message)
    }


    //////// Старий метод
    // const user = new User(req.body)
    // const updateObject = {
    //     new:true,
    //     useFindAndModify:false
    // }


    // User.findByIdAndUpdate(id, user, { // переробить
    //     new:true,
    //     useFindAndModify:false
    // }).then(()=>{
    //     res.status(200).send("User update")
    // }).catch((err)=>{
    //     res.status(400).send(err.message)
    // })

})

router.post("/users/login", async (req, res) => {
    try{
        const user = await User.findOneByCredentials(req.body[0].email, req.body[0].password);       
        const token = await user.generateAuthToken(); 
        res.send({user, token});
    }catch(e){
        res.status(400).send();
    }
})

router.post('/users/logout', auth, async (req, res) => {
    try{
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token != req.token;
        })
        await req.user.save();
        res.send();
    }catch(e){
        res.status(500).send()
    }
})




router.delete("/users/:id", (req, res) =>{
    const id = req.params.id 
    User.findByIdAndDelete(id).then((users) => {
        res.status(200).send("User delete")
    }).catch((err) => {
        res.status(400).send(err.message)
    })
 } )

 router.get("/users/:id", async (req, res) =>{ // Новий метод
    try{
        let user = await User.findById(req.params.id)
        res.status(200).send(user)
    }catch(err){
        res.status(500).send(err.message)
    }
})

// lab 4 

router.post("/users/logoutAll", auth, async(req, res) => {
    try{
        req.user.tokens = [];
        await req.user.save();
        res.status(200).send("Logout from all devices");
    }catch(e){
        res.status(500).send(e.message);
    }
});

router.delete("/users/me", auth, async(req, res) => {
    try{
        await req.user.remove();
        res.send("req.user");
    }catch(e){
        res.status(500).send(e.message);
    }
});

module.exports = router;