// const request = require("request")
// const hbs = require('hbs')


const express = require('express')
const fs = require('fs');
// const mongoose = require('mongoose')
const validator = require('validator')
const jsonParse = express.json()
// MODEL
require('./db/mongoose')
require('./model/user')
require('./model/task')

const Task = require('mongoose').model('Task')
const UserRouter = require('./routers/user')
const TaskRouter = require('./routers/task')

let app = express();

app.use(express.json()) // 
app.use(UserRouter)
app.use(TaskRouter)




app.listen(3000, () => { // запуск на порті
    console.log("Example app listening on port 3000");
    
});