const mongoose = require('mongoose')
let TaskSchema = new mongoose.Schema({
    description:{
        type:String,
        required:true,
        trim:true
    },
    completed:{
        type:Boolean,
        default: false
    },
    title: {
        type: String,
        default: false
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "User"
    }
})


const Task = mongoose.model('Task', TaskSchema)

module.exports = Task



// lab 4

// const main = async () => {
//     const task = await Task.findById('5eb27f67c944ef390c143b3b')
//     await task.populate('owner').execPopulate();
//     console.log(task.owner);
// }

// main();
