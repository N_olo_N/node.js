const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')


let UserSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true,
        trim: true
    },
    age:{
        type: Number,
        default:0,
        validate(value){
            if(value < 0)
                throw new Error('Age must be positive number')
        }
    },
    email:{
        type: String,
        required: true,
        lowercase:true,
        unique:true,
        validate(value){
            if(!require('validator').isEmail(value)){
                throw new Error('Email is invalid')
            }
        }
    },
    password:{
        type:String,
        required:true,
        minlength:7,
        trim:true,
        validate(value){
            if(value.indexOf('password') !== -1 )
                throw new Error('Password is invalid')
        }
    },
    tokens : [{
        token: {
            type: String,
            require: true
        }
    }]
})

////
UserSchema.pre('save', async function(next) {
    const user = this

    if (user.isModified('password')) {
         user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})

UserSchema.statics.findOneByCredentials = async (email, password) => {
    const user = await User.findOne({email});

    if (!user) {
        throw new Error('Incorrect email');
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
        throw new Error('Incorrect password');
    }

    return user;
}

UserSchema.methods.generateAuthToken = async function () {
    const user = this;

    const token = jwt.sign({_id: user._id.toString()}, 'gdefjrudlsngjsrvndj');

    user.tokens = user.tokens.concat({token});

    await user.save();

    return token;
}
////


// lab 4

UserSchema.methods.toJSON = function(){
    const user = this;
    const userObject = user.toObject();
    delete userObject.password;
    delete userObject.token;
    return userObject;
}

UserSchema.virtual('tasks', {
    ref: "Task",
    localField: '_id',
    foreignField: 'owner'
})


const User = mongoose.model('User', UserSchema)

module.exports = User


// const main = async () => {
//     const user = await User.findById('5e6511db04e7ed25108bc255')
//     await user.populate("tasks").execPopulate();
//     console.log(user.tasks);
// }

// main();















// const mongoose = require('mongoose')

// let UserSchema = { // схема 
//     firstName: {
//         type: String,
//         require: true,
//         trim: true
//     },
//     age: {
//         type: Number,
//         require: true,
//         validate(value){
//             if (value < 0) {
//                 throw new Error("Age must be a positive number")
//             }
//         }
//     },
//     password: {
//         type: String,
//         require: true
//     }
// }

// mongoose.model('User', UserSchema) // модель

// let user1 = new User({firstName: "Alex", age: 39}) // екземпляр

// user1.save().then(() => {
//     console.log("Successful");
// }).catch((err) => {
//     console.log(err.message);
    
// }) // зберігаємо! проміс