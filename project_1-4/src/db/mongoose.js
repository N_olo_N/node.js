const mongoose = require('mongoose')

var conn = 'mongodb://127.0.0.1:27017/task-manager-api'

mongoose.connect(conn, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
})