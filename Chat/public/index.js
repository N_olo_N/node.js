typing = false
timeout = undefined
to = document.getElementById('to')
document.getElementById('message').onchange = onKeyDown
form = document.getElementById('form')
chat = document.getElementById('chat')
messages = document.getElementById('messages')
users = document.getElementById('users')
username = ''
room = ''
var socket = io()
document.getElementById('butConnect').onclick = () =>{
    username = document.getElementById('login').value
    room = document.getElementById('room').value
    socket.emit('join',({username, room}))
    form.style.display='none'
    chat.style.display='grid'
    document.getElementById('username').innerText = username
    document.getElementById('roomname').innerText = room
}
document.getElementById('send').onclick = () => {
    message = document.getElementById('message').value
    document.getElementById('message').value = ''
    if(to.value == '0'){
        socket.emit('newMessage',{username,message})
    }
    else{
        socket.emit('private',{message: message, to: to.value, from:username})
    }
    
}
document.getElementById('leave').onclick = () => {
    socket.emit('leave')
    messages.innerHTML = ''
    chat.style.display= 'none'
    form.style.display = 'flex'
    
}
socket.on('private', (data) => {
    messages.innerHTML += "<div class='message'><h5>"+data.from+"</h5><p>"+data.message+"</p><span class='time'>"+data.time+" PRIVATE </span></div>"
})
socket.on('message',(message) => {
    messages.innerHTML += "<div style='display: flex; justify-content: center;'><p class='event'>"+message+"</p></div>"
})
socket.on('newMessage',({username,message,time}) => {
    messages.innerHTML += "<div class='message'><h5>"+username+"</h5><p>"+message+"</p><span class='time'>"+time+"</span></div>"
})
socket.on('users',(userslist) => {

    users.innerHTML = ''
    
    to.innerHTML = "<option value = '0'>Всім</option>"
    userslist.forEach(el => {
        users.innerHTML += "<li>"+el.username+"</li>"
        to.innerHTML += "<option value ='"+el.id+"'>"+el.username+"</option>" 
    })

})
socket.on('nowTyping',(list) =>{

    nowTyping = document.getElementById('typing')
    if(list.length > 1)
        nowTyping.innerText = list.join(', ')+ " пишуть..."
    else if(list.length == 1)
        nowTyping.innerText = list[0]+ " пише..."
    else
        nowTyping.innerText = ''

})
function timeoutFunc(){
    typing = false
    socket.emit('timeout',username)
}
function onKeyDown(){    
    if(typing == false){
        typing = true
        socket.emit('typing',username)
        timeout = setTimeout(timeoutFunc,1000)
    }
    else{
        clearTimeout(timeout)
        timeout = setTimeout(timeoutFunc,1000)
    }
}

////////////////
document.getElementById('message').oninput  = () => {
    if(typing == false){
        typing = true
        socket.emit('typing',username)
        timeout = setTimeout(timeoutFunc,1000)
    }
    else{
        clearTimeout(timeout)
        timeout = setTimeout(timeoutFunc,1000)
    }
}