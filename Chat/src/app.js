const express = require('express')
const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)

app.use(express.static(`${__dirname}/../public`))
typing = []
io.on('connection', (socket) => {
    socket.on('join',({username, room}) => {
        socket.join(room)
        socket.username = username
        io.to(room).emit('message',username + ' приєднався')
        updateUsers(room)
        socket.on('newMessage',({username,message}) => {
            time = new Date().toLocaleString('ru',{hour:'numeric',minute:'numeric'})
            io.to(room).emit('newMessage',{username,message,time})
        })
        socket.on('leave', () => {
            socket.leave(room)
            io.to(room).emit('message',username + ' вийшов') 
            updateUsers(room)
        })
        socket.on('typing', (username) => {
            typing.push(username)
            io.to(room).emit('nowTyping',typing)
        })
        socket.on('timeout', (username) => {
            typing.splice(typing.indexOf(username),1)
            io.to(room).emit('nowTyping',typing)
        })
        socket.on('private', (data) => {
            console.log(data)
            time = new Date().toLocaleString('ru',{hour:'numeric',minute:'numeric'})
            io.sockets.sockets[data.to].emit('private',{from: data.from, message: data.message, time:time})
            if(data.to != socket.id)
                io.sockets.sockets[socket.id].emit('private',{from: data.from, message: data.message, time:time})
            
        })
        
    })
})
updateUsers = (room) => {
    io.of('/').adapter.clients([room], (err, clients) => {
        users = []
        clients.forEach(element => {
            users.push({username: io.sockets.connected[element].username, id: element})
        });
        io.to(room).emit('users',users)
      });
}
server.listen(3000, () =>{
    console.log('listening on 3000 port')
})