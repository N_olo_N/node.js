const mongoose = require('mongoose')
mongoose.connect(`mongodb+srv://user:12345@bd-lr-5dypf.gcp.mongodb.net/test?retryWrites=true&w=majority`,{
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology:true
})
let userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true,
        trim: true,
        minlength: 2
    },
    age:{
        type: Number,
        required: true,
        default:0,
        validate(value){
            if(value < 0)
                throw new Error('Age must be positive number')
        }
    },
    email:{
        type:String,
        required: true,
        lowercase:true,
        unique:true,
        validate(value){
            if(!require('validator').isEmail(value)){
                throw new Error('Email is invalid')
            }
        }
    }
    
})




const mochaUser = mongoose.model("mochaUser", userSchema)

module.exports = mochaUser

