const functions = require('./modules/functions')
const User = require('./modules/user')
const assert = require('assert')
let users = [
    {name: 'Bohdan', age: 12, email: 'bohdan@gmail.com' },
    {name: 'Jake', age: '18' },
    {name: 'Jone', age: -5, email: 'jone@gamil.com' },
    {name: 'Petro', age: 4, email: 'petro@gamil.com' },
    {name: 'Bob', age: 7, email: 'bob@gamil.com' },
    {name: 'Kola', age: 9, email: 'gamil.comgamil.comgamil.com' },
    { age: 10, email: '10@gamil.com' },
    {name: 'X Æ A-12',  email: 'ilonmask@gamil.com' }
]
it('adding two numbers', function(){
    var correctResult = 8
    var currentResult = functions.add(3,5)
    if(correctResult !== currentResult)
        throw new Error(`Your result is ${currentResult}, correct result should be ${correctResult}`)
})
describe('factorial', function(){
    it('F(0)',function(){
        assert.equal(functions.factorial(0),1)
    })
    it('F(-5)',function(){
        assert.equal(functions.factorial(-5),-120)
    })
    it('F(6)',function(){
        assert.equal(functions.factorial(6),720)
    })
})
describe('User',function(){
    for(let i = 0; i< users.length; i++){
        it(`User ${i} is testing`,function(done){
            var user = new User()
            user.name = users[i].name
            user.age = users[i].age
            user.email = users[i].email
            user.save(done)
        })
    }
})
